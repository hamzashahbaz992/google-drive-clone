﻿using MD.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThumbnailSharp;

namespace MD.DAL
{
    public static class FileDAO
    {

        public static List<FileDTO> GetAllFilesByUserId(int UserId) 
        {
            var query = String.Format("Select * from dbo.Files Where CreatedBy = {0} AND IsActive = '{1}';", UserId, true);

            using (DBHelper helper = new DBHelper())
            {
                List<FileDTO> list = null;
                var reader = helper.ExecuteReader(query);
                
                if (reader.HasRows)
                {
                    list = new List<FileDTO>();
                    while (reader.Read())
                    {
                        FileDTO dto = FillDTO(reader);
                        if (dto != null)
                        {
                            if (dto.IsActive)
                                list.Add(dto);
                        }
                    }
                }
                return list;
            }
        }
        public static List<FileDTO> GetFilesByParentFolderId(FileDTO dto)
        {
            var query = String.Format("Select * from dbo.Files Where ParentFolderId = {0} AND CreatedBy = {1} AND IsActive = '{2}';", dto.ParentFolderId, dto.CreatedBy, dto.IsActive);

            using (DBHelper helper = new DBHelper())
            {
                var reader = helper.ExecuteReader(query);
                List<FileDTO> list = null;
                if (reader.HasRows)
                {
                    list = new List<FileDTO>();
                    while (reader.Read())
                    {
                        dto = FillDTO(reader);
                        if (dto != null)
                        {
                            if (dto.IsActive)
                                list.Add(dto);
                        }
                    }
                }
                return list;
            }
        }

        public static FileDTO GetFileById(FileDTO dto)
        {
            var query = String.Format("Select * from dbo.Files Where Id = {0} AND CreatedBy = {1} AND IsActive = '{2}';", dto.Id, dto.CreatedBy, dto.IsActive);
            using (DBHelper helper = new DBHelper())
            {
                var reader = helper.ExecuteReader(query);
                dto = null;

                if (reader.Read())
                {
                    dto = FillDTO(reader);
                }
                return dto;
            }
        }

        public static FileDTO GetFileByName(FileDTO dto)
        {
            var query = String.Format("Select * from dbo.Files  Where Name = {0} AND CreatedBy = {1} AND IsActive = '{2}';", dto.Name, dto.CreatedBy, dto.IsActive);
            using (DBHelper helper = new DBHelper())
            {
                var reader = helper.ExecuteReader(query);
                dto = null;

                if (reader.Read())
                {
                    dto = FillDTO(reader);
                }
                return dto;
            }
        }

        public static bool DeleteFileById(FileDTO dto, String rootPath)
        {
         //   deleteFile(MD.DAL.FileDAO.GetFileById(dto), rootPath); // hard delete
            var query = String.Format("Update dbo.Files Set IsActive = 'False' Where Id = {0} AND CreatedBy = {1} AND IsActive = '{2}';", dto.Id, dto.CreatedBy, dto.IsActive);
            using (DBHelper helper = new DBHelper())
            {
                helper.ExecuteScalar(query);

                return true;
            }
        }

        public static FileDTO Save(FileDTO dto, String rootPath, Stream fileInputStream)
        {
            if (!writeFile(dto, rootPath, fileInputStream))
            {
                return null;
            }

            var query = "";
            using (DBHelper helper = new DBHelper())
            {
                if (dto.Id > 0)
                {
                    query = String.Format("Update dbo.Files SET Name = '{0}' WHERE ParentFolderId = {1} AND Id = {2}, IsActive = '{2}')", dto.Name, dto.ParentFolderId, dto.Id, dto.IsActive);
                    helper.ExecuteScalar(query);
                    return dto;
                }
                else
                {

                    query = String.Format("INSERT INTO dbo.Files (Name, ParentFolderId, FileExt, FileSizeInKb, UploadedOn, IsActive, CreatedBy) VALUES ('{0}', {1}, '{2}', {3}, '{4}', '{5}', {6})", dto.Name, dto.ParentFolderId, dto.FileExt, dto.FileSizeInKb, dto.UploadedOn, dto.IsActive, dto.CreatedBy);
                    query += "; Select Scope_Identity()";
                    dto.Id = Convert.ToInt32(helper.ExecuteScalar(query));
                    return dto;
                }

            }
        }

        private static bool deleteFile(FileDTO dto, String rootPath)
        {
            if (dto != null)
            {
                var filePath = rootPath + dto.Name + dto.FileExt;
                var thumbPath = rootPath + "thumb_" + dto.Name + ".jpg";

                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
                if (File.Exists(thumbPath))
                {
                    File.Delete(thumbPath);
                }
            }
            return true;
        }

        private static bool writeFile(FileDTO dto, String rootPath, Stream fileInputStream)
        {
            string parentFolderPath = rootPath + dto.ParentFolderId + @"\";

            if (!Directory.Exists(parentFolderPath))
            {
                Directory.CreateDirectory(parentFolderPath);
            }

            var currentFileName = String.Format("{0}{1}", parentFolderPath, dto.Name + dto.FileExt);
            if (!File.Exists(currentFileName))
            {
                using (var fileStream = File.Create(String.Format("{0}{1}", parentFolderPath, dto.Name + dto.FileExt)))
                {
                    fileInputStream.Seek(0, SeekOrigin.Begin);
                    fileInputStream.CopyTo(fileStream);
                }

                fileInputStream.Seek(0, SeekOrigin.Begin);
                using(Stream resultThumbStream = new ThumbnailCreator().CreateThumbnailStream(
                    thumbnailSize: 100,
                    imageStream: fileInputStream,
                        imageFormat: Format.Jpeg
                )) {

                    using (var thumbStream = File.Create(String.Format("{0}{1}", parentFolderPath, "thumb_"  + dto.Name + ".jpg")))
                    {
                        resultThumbStream.Seek(0, SeekOrigin.Begin);
                        resultThumbStream.CopyTo(thumbStream);
                        return true;
                    }

                }
            }
            else
            {
                return false;
            }

        }


        private static FileDTO FillDTO(SqlDataReader reader)
        {
            var dto = new FileDTO();
            dto.Id = reader.GetInt32(reader.GetOrdinal("Id"));
            dto.Name = reader.GetString(reader.GetOrdinal("Name"));
            dto.ParentFolderId = reader.GetInt32(reader.GetOrdinal("ParentFolderId"));
            dto.FileExt = reader.GetString(reader.GetOrdinal("FileExt"));
            dto.FileSizeInKb = reader.GetInt32(reader.GetOrdinal("FileSizeInKb"));
            dto.UploadedOn = reader.GetDateTime(reader.GetOrdinal("UploadedOn"));
            dto.IsActive = reader.GetBoolean(reader.GetOrdinal("IsActive"));
            dto.CreatedBy = reader.GetInt32(reader.GetOrdinal("CreatedBy"));

            return dto;
        }
    }
}
