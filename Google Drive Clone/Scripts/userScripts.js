﻿
$(document).ready(function () {
    
    /*$('.item-file-folder').click(function () {
        //$(this).toggleClass('active');
    });*/

    $('.item-file-folder').bind("contextmenu", function () {
        $('.item-file-folder').removeClass('active');
        $(this).addClass('active');
    }).attr('unselectable', 'on');
    $(window).click(function () {
        $('.item-file-folder').removeClass("active");
    });
    $('.item-file-folder').dblclick(function () {
        alert($(this).text());
    }).css('user-select', 'none').on('selectstart', false);

    $('#btnDownloadMeta').click(function (e) {
		var link = '/api/Ajax/GeneratePDF?UserId=' + localStorage.getItem('UserId');
		$('#fileDownloadLink').attr('href', link);
		window.location = link;
		$('#modalDownload').modal('show');
		e.preventDefault();
    });

	
	// SEARCH
	
	$('#inputSearch').on('keyup', function() {
		var keyword =  $(this).val().toLowerCase();
		var items = $('#explorer').children();
		
		var itemsCount = 0;
		
		for(var i = 0; i < items.length; i++) {
			var e = items[i];
			
			
			var itemName = $(e).find('.item-title').text().toLowerCase();
			if(itemName.indexOf(keyword) != -1) {
				// found
					$(e).fadeIn(function() {$(this).addClass('d-inline-flex').removeClass('d-none')});
					itemsCount++;
				
			} else {
					$(e).fadeOut(function() {$(this).removeClass('d-inline-flex').addClass('d-none')});
			}
			
			if(i+1 == items.length) {
				// last index
				if(itemsCount == 0) {
					$('#noitems').slideDown();
				} else if($('#noitems').is(':visible')) {
					$('#noitems').slideUp();
				}	
			}
		}
		
	
			
	});
	
			
});
function showToast(message, timeout) {
	if(!timeout) {
		timeout = 3000;
	}
    // Get the snackbar DIV
	var x = $('<div class="show" id="snackbar">');


	
	x.html(message).appendTo($('body'));

    // Add the "show" class to DIV
    //x.className = "show";

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.removeClass('show').remove(); }, timeout);
} 


const HTML_COMPONENTS = {
    ITEM_CLASS: 'item-file-folder',
    ITEM_FOLDER_CLASS: 'item-folder',
    ITEM_FILE_CLASS: 'item-file',

    getItemHTML: function (e) {
		console.log(e);
        if(e.type == 'folder') {
			return this.getFolderItemHTML(e);
		} else {			
			return this.getFileItemHTML(e);
		}
    },
	
    getFolderItemHTML: function (e) {
        var $mainDiv = $('<div style="display:none;" class="' + this.ITEM_CLASS + ' d-inline-flex m-2 ' + this.ITEM_FOLDER_CLASS + '" data-itemType="folder" data-itemId="' + e.Id + '">');
        var $cardDiv = $('<div class="card flex-row">');
        var $imageDiv = $('<div><img src="'+ DOMAIN + '/Content/images/folder-icon.png" class="img-fluid align-self-center item-thumb-img" />');
        var $titleDiv = $('<div class="align-self-center p-2 item-title"><span class="small">' + e.Name + '</span></div>');
        return $mainDiv.append($cardDiv.append($imageDiv).append($titleDiv));
    },
    getFileItemHTML: function (e) {
        var $mainDiv = $('<div style="display:none;" class="' + this.ITEM_CLASS + ' d-inline-flex m-2 ' + this.ITEM_FILE_CLASS + '" data-itemType="file" data-itemId="' + e.Id + '">');
        var $cardDiv = $('<div class="card flex-row">');
        var $imageDiv = $('<div><img src="' + DOMAIN + '/uploads/' + localStorage.getItem("UserLogin") + '/' + PRESENT_FOLDER.get() + '/thumb_' + e.Name + '.jpg" class="img-fluid align-self-center item-thumb-img" />');
        var $titleDiv = $('<div class="align-self-center p-2 item-title"><span class="small">' + e.Name + '</span></div>');
        return $mainDiv.append($cardDiv.append($imageDiv).append($titleDiv));
    }
};

const DOMAIN = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '');
const AJAX_METHODS = {

    API_URL: DOMAIN + '/api',
	POST: function(actionName, data, success_callback, error_callback) {
		console.log('calling api: ' + actionName);
		console.log('data: ', data);
        var ajaxSettings = {
            type: 'POST',
            url: this.API_URL + '/Ajax/' + actionName,
			contentType: "application/json; charset=utf-8",
            data: data,
            success: function (res) {
				if(res.success) {	
					if(typeof success_callback == 'function') 
						success_callback(res.result);
				} else {
					if(typeof error_callback == 'function') 
						error_callback(res);
					
					showToast(res.Message);
				}
            },
            error: function (res) {
				if(typeof error_callback == 'function') 
					error_callback(res);
				
				showToast(res.Message);
            }
        }
        $.ajax(ajaxSettings);
	},
	
	
	
    /*
    It will fetch array of items passed in param of cb function
    */
    getItemsByParentFolderId: function (pParentFolderId, pUserId, success_callback, error_callback) {
		var data = JSON.stringify({
                ParentFolderId: pParentFolderId,
                CreatedBy: pUserId,
                IsActive: true
            });
		this.POST('getItemsByParentFolderId', data, success_callback, error_callback);
    },
	
	
	
    /*
    It will fetch array of folders passed in param of cb function
    */
    getFoldersByParentFolderId: function (pParentFolderId, pUserId, success_callback, error_callback) {
		var data = JSON.stringify({
                ParentFolderId: pParentFolderId,
                CreatedBy: pUserId,
                IsActive: true
            });
		this.POST('GetFoldersByParentFolderId', data, success_callback, error_callback);
    },
	
    /*
    It will fetch folder passed in param of cb function
    */
	getFolderById: function (pFolderId, pUserId, success_callback, error_callback) {
		var data = JSON.stringify({
                Id: pFolderId,
                CreatedBy: pUserId,
                IsActive: true
            });
		this.POST('GetFolderById', data, success_callback, error_callback);
	},
	
	
	deleteFolderById: function (pFolderId, pUserId, success_callback, error_callback) {
		var data = JSON.stringify({
                Id: pFolderId,
                CreatedBy: pUserId,
                IsActive: true
            });
		this.POST('DeleteFolderById', data, success_callback, error_callback);
	},
	
	deleteFileById: function (pFileId, pUserId, success_callback, error_callback) {
		var data = JSON.stringify({
                Id: pFileId,
                CreatedBy: pUserId,
                IsActive: true
            });
		this.POST('DeleteFileById', data, success_callback, error_callback);
	},
	
	saveFolder: function(pParentFolderId, pFolderName, pUserId, success_callback, error_callback) {
		var data = JSON.stringify({
				ParentFolderId: pParentFolderId,
                Name: pFolderName,
                CreatedBy: pUserId,
                IsActive: true
            });
		this.POST('SaveFolder', data, success_callback, error_callback);
	},
	
	
    /*
    It will fetch array of files passed in param of cb function
    */
    getFilesByParentFolderId: function (pParentFolderId, pUserId, success_callback, error_callback) {
		var data = JSON.stringify({
                ParentFolderId: pParentFolderId,
                CreatedBy: pUserId,
                IsActive: true
            });
		this.POST('GetFilesByParentFolderId', data, success_callback, error_callback);
    },
	
    /*
    It will fetch file passed in param of cb function
    */
	getFileById: function (pFileId, pUserId, success_callback, error_callback) {
		var data = JSON.stringify({
                Id: pFileId,
                CreatedBy: pUserId,
                IsActive: true
            });
		this.POST('GetFileById', data, success_callback, error_callback);
	},
	
    saveFile: function (data, success_callback, error_callback) {
		console.log('calling api: SaveFile');
		console.log('data: ', data);
        var ajaxSettings = {
            type: 'POST',
            url: this.API_URL + '/Ajax/SaveFile',
			contentType: false,
			processData: false,
            data: data,
            success: function (res) {
				if(res.success) {	
					if(typeof success_callback == 'function') 
						success_callback(res.result);
				} else {
					if(typeof error_callback == 'function') 
						error_callback(res);
				}
				console.log(res);
            },
            error: function (res) {
				if(typeof error_callback == 'function') 
					error_callback(res);
            }
        }
        $.ajax(ajaxSettings);
	}
};

var CONTEXTMENU_FUNCTIONS = {
	FolderDetails: function (key, opt) {
		var itemId = $(opt.$trigger).data('itemid');
		AJAX_METHODS.getFolderById(itemId, localStorage.getItem("UserId"), function(item) {
			$('#modalDetails .details').removeClass('filedetails').addClass('folderdetails');
			$('#modalDetails .details .Type').html(item.type);
			$('#modalDetails .details .Name').html(item.Name);
			$('#modalDetails .details .Owner').html(localStorage.getItem("UserName"));
			$('#modalDetails .details .CreatedOn').html(item.CreatedOn);
			$('#modalDetails').modal('show');
		});
	},
	FileDetails: function (key, opt) {
		var itemId = $(opt.$trigger).data('itemid');
		AJAX_METHODS.getFileById(itemId, localStorage.getItem("UserId"), function(item) {
			console.log(item);
			$('#modalDetails .details').removeClass('folderdetails').addClass('filedetails');
			$('#modalDetails .details .Type').html(item.type);
			$('#modalDetails .details .Name').html(item.Name);
			$('#modalDetails .details .Ext').html(item.FileExt);
			$('#modalDetails .details .FileSizeInKb').html(item.FileSizeInKb + ' kb');
			$('#modalDetails .details .Owner').html(localStorage.getItem("UserName"));
			$('#modalDetails .details .CreatedOn').html(item.UploadedOn);
			$('#modalDetails').modal('show');
		});
	},
	FilePreview: function (key, opt) {
	    var itemId = $(opt.$trigger).data('itemid');
	    CONTEXTMENU_FUNCTIONS.FilePreviewModal(itemId);
	},
	FilePreviewModal: function(itemId) {
		AJAX_METHODS.getFileById(itemId, localStorage.getItem("UserId"), function(item) {
			console.log(item);
			var fileUrl = '/uploads/' + localStorage.getItem('UserLogin') + '/' + item.ParentFolderId + '/' + item.Name + item.FileExt;
			$('#modalPreview img').attr('src', fileUrl);
			$('#modalPreview').modal('show');
		});
	},
	RefreshExplorer: function () {
	    refreshExplorer(PRESENT_FOLDER.get());
	}
};
var MODAL_FUNCTIONS = {
	FolderDelete: function (itemId) {
		AJAX_METHODS.deleteFolderById(itemId, localStorage.getItem("UserId"), function(item) {
			refreshExplorer(PRESENT_FOLDER.get());
			$('#modalDelete').modal('hide');
			
			showToast('Folder successfully deleted ');
		});
	},
	
	FolderCreate: function (itemName) {
		
		AJAX_METHODS.saveFolder(PRESENT_FOLDER.get(), itemName, localStorage.getItem("UserId"), function(item) {
			refreshExplorer(PRESENT_FOLDER.get());
			$('#modalNewFolder').modal('hide');

			$('#formNewFolder').attr('novalidate', 'novalidate');
			$('#formNewFolder').get(0).reset();
			$('#formNewFolder').removeAttr('novalidate');

			showToast('Folder successfully created');
		});
	},
	
	FileDelete: function (itemId) {
		AJAX_METHODS.deleteFileById(itemId, localStorage.getItem("UserId"), function(item) {
			refreshExplorer(PRESENT_FOLDER.get());
			$('#modalDelete').modal('hide');
			
			showToast('File successfully deleted ');
		});
	},
	
	UploadFile: function (files) {
	    var data = new FormData();
		if(files.length > 0) {
			data.append('file', files[0]);
		}
		data.append('ParentFolderId', PRESENT_FOLDER.get());
		data.append('CreatedBy', localStorage.getItem("UserId"));
		
		AJAX_METHODS.saveFile(
		data,
		function () {
		    console.log("saveFile is called");
		    refreshExplorer(PRESENT_FOLDER.get());
		    $('#modalUpload').modal('hide');

		    $('#formUpload').attr('novalidate', 'novalidate');
		    $('#formUpload').get(0).reset();
		    $('#formUpload').removeAttr('novalidate');

			showToast('File successfully uploaded');
		}, function(result) {
			showToast(result.Message);
		});
	}
}
var PRESENT_FOLDER = {
	set: function (folderId, cb) {
		$('#explorer').data('presentFolderId', parseInt(folderId));
		if(typeof cb == 'function') {
			cb(folderId);
		}
	},
	get: function () {
		return $('#explorer').data('presentFolderId');
	}
}


var PANEL = {
	$btnHome: $('#panel-btn-home'),
	$btnBack: $('#panel-btn-back'),
	$btnForward: $('#panel-btn-forward'),
	$btnUp: $('#panel-btn-up'),
	
	breadcrumb: { 
		backBucket: [],
		forwardBucket: [],
		upBucket: [],
	},
	init: function() {
		$(this.$btnHome).click(function() {
			if(PRESENT_FOLDER.get() != 0) {
				PANEL.breadcrumb.backBucket.push(PRESENT_FOLDER.get());
				PANEL.breadcrumb.upBucket = [];
				PANEL.breadcrumb.forwardBucket = [];
				refreshExplorer(0);
				PANEL.$btnHome.attr('disabled', 'disabled').addClass('disabled');
			}
		});
		
		$(this.$btnBack).click(function() {
			console.log('Back btn click');
			if(PANEL.breadcrumb.backBucket.length > 0) {
				
				var folderId = PANEL.breadcrumb.backBucket.pop();
				
				if(folderId == 0) {
					PANEL.breadcrumb.upBucket = [];
				}
				
				PANEL.breadcrumb.forwardBucket.push(PRESENT_FOLDER.get());
				
				refreshExplorer(folderId);
			}
		});
		$(this.$btnForward).click(function() {
			console.log('btnForward btn click');
			if(PANEL.breadcrumb.forwardBucket.length > 0) {
				
				var folderId = PANEL.breadcrumb.forwardBucket.pop();
				
				if(folderId == 0) {
					PANEL.breadcrumb.upBucket = [];
				}
				
				PANEL.breadcrumb.backBucket.push(PRESENT_FOLDER.get());
				
				refreshExplorer(folderId);
				
			}
		});
		$(this.$btnUp).click(function() {
			if(PANEL.breadcrumb.upBucket.length > 0) {
				
				PANEL.breadcrumb.forwardBucket = [];
				
				var folderId = PANEL.breadcrumb.upBucket.pop();
				
				PANEL.breadcrumb.backBucket.push(PRESENT_FOLDER.get());
				
				refreshExplorer(folderId);
			}
		});
	},
	update: function() {
		if(PRESENT_FOLDER.get() == 0) {
			this.$btnHome.attr('disabled','disabled').addClass('disabled');
		} else {
			this.$btnHome.removeAttr('disabled').removeClass('disabled');
		}
		// UP BUCKET
		if(this.breadcrumb.upBucket.length == 0) {
			this.$btnUp.attr('disabled','disabled').addClass('disabled');
		} else {
			this.$btnUp.removeAttr('disabled').removeClass('disabled');
		}
		
		// BACK BUCKET
		if(this.breadcrumb.backBucket.length == 0) {
			
			this.$btnBack.attr('disabled','disabled').addClass('disabled');
		} else {
			this.$btnBack.removeAttr('disabled').removeClass('disabled');
		}
		
		// FORWARD BUCKET
		if(this.breadcrumb.forwardBucket.length == 0) {
			this.$btnForward.attr('disabled','disabled').addClass('disabled');
		} else {
			this.$btnForward.removeAttr('disabled').removeClass('disabled');
		}
		
		// UP BUCKET
		if(this.breadcrumb.upBucket.length == 0) {
			this.$btnUp.attr('disabled','disabled').addClass('disabled');
		} else {
			this.$btnUp.removeAttr('disabled').removeClass('disabled');
		}
		
		
		
	}
}

function isEmptyExplorer(itemsCount) {
	if($('#explorer').html() == "") {
		$('#noitems').slideDown();
	} else if($('#noitems').is(':visible')) {
		$('#noitems').slideUp();
	}
}

function initExplorer(parentFolderId) {
    AJAX_METHODS.getItemsByParentFolderId(
	parentFolderId, 
	localStorage.getItem("UserId"), 
	function (result) {
		
		PRESENT_FOLDER.set(parentFolderId, function(folderId) {			
			PANEL.init();
		});
		
		$('#explorer').empty();
		
		$.each(result, function(i, e) {
			var item = HTML_COMPONENTS.getItemHTML(e);
			$('#explorer').append(item);
			item.fadeIn((i+1)*200);
		});
		
		isEmptyExplorer();
    }, 
	function(result) {	
			console.log(result);
			isEmptyExplorer();
	});
}

function refreshExplorer(parentFolderId) {

    AJAX_METHODS.getItemsByParentFolderId(
	parentFolderId, 
	localStorage.getItem("UserId"), 
	function (result) {
		PRESENT_FOLDER.set(parentFolderId, function(folderId) {	
			PANEL.update();
		});
		$('#explorer').empty();
		
		$.each(result, function(i, e) {
			var item = HTML_COMPONENTS.getItemHTML(e);
			$('#explorer').append(item);
			item.fadeIn((i+1)*200);
		});
				
		isEmptyExplorer();
    }, 
	function(result) {
		isEmptyExplorer();
	});
	
}

function openFolder(folderId) {
	PANEL.breadcrumb.upBucket.push(PRESENT_FOLDER.get());	
	PANEL.breadcrumb.backBucket.push(PRESENT_FOLDER.get());
	PANEL.breadcrumb.forwardBucket = [];
	PANEL.update();
	refreshExplorer(folderId);
}
$(document).ready(function () {
    initExplorer(0);
    $(document).on('dblclick', '.' + HTML_COMPONENTS.ITEM_FOLDER_CLASS, function () {
        var folderId = $(this).attr('data-itemid');
        openFolder(folderId);
    });
    $(document).on('click', '.' + HTML_COMPONENTS.ITEM_FILE_CLASS, function () {
        var fileId = $(this).attr('data-itemid');
        CONTEXTMENU_FUNCTIONS.FilePreviewModal(fileId);
    });
});
