﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MD.Entities;

namespace MD.DAL
{
    public static class UserDAO
    {
        public static List<UserDTO> GetAllUsers()
        {

            var query = "Select * from dbo.Users";

            using (DBHelper helper = new DBHelper())
            {
                var reader = helper.ExecuteReader(query);

                List<UserDTO> list = new List<UserDTO>();

                while (reader.Read())
                {
                    UserDTO dto = FillDTO(reader);
                    list.Add(dto);
                }

                return list;
            }
        }

        public static UserDTO GetUserById(int pId)
        {
            var query = String.Format("Select * from dbo.Users Where Id={0} ", pId);

            using (DBHelper helper = new DBHelper())
            {
                var reader = helper.ExecuteReader(query);

                UserDTO dto = null;

                if (reader.Read())
                {
                    dto = FillDTO(reader);
                }

                return dto;
            }
        }
        public static UserDTO ValidateUser(String pLogin, String pPassword)
        {
            var query = String.Format("Select * from dbo.Users Where Login='{0}' and Password='{1}'", pLogin, pPassword);

            using (DBHelper helper = new DBHelper())
            {
                var reader = helper.ExecuteReader(query);

                UserDTO dto = null;

                if (reader.Read())
                {
                    dto = FillDTO(reader);
                }

                return dto;
            }
        }

        private static UserDTO FillDTO(SqlDataReader reader)
        {
            var dto = new UserDTO();
            dto.Id = reader.GetInt32(reader.GetOrdinal("Id"));
            dto.Name = reader.GetString(reader.GetOrdinal("Name"));
            dto.Login = reader.GetString(reader.GetOrdinal("Login"));
            dto.Password = reader.GetString(reader.GetOrdinal("Password"));
            dto.Email = reader.GetString(reader.GetOrdinal("Email"));

            return dto;
        }


    }
}
