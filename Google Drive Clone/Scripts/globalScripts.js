﻿function showAlert(el, message, success) {
    if ($(el).length == 0) {
        console.log('No alert div exists');
        return;
    }

    if (success) {
        $(el).slideUp(function () {
            $(this).html(message).removeClass('alert-danger').addClass('alert-success').slideDown();
        });
    } else {
        $(el).slideUp(function () {
            $(this).html(message).removeClass('alert-success').addClass('alert-danger').slideDown()
        });
    }
}