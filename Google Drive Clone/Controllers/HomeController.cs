﻿using Assignment_8.Security;
using MD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_8.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (SessionManager.IsValidUser)
            {
                return Redirect("~/User/Index");
            }
            return View();
        }
    }
}