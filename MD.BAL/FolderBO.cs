﻿using MD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MD.BAL
{
    public class FolderBO
    {
        public static List<FolderDTO> GetAllFoldersByUserId(int UserId)
        {
            return MD.DAL.FolderDAO.GetAllFoldersByUserId(UserId);
        }
        public static FolderDTO Save(FolderDTO dto)
        {
            return MD.DAL.FolderDAO.Save(dto);
        }

        public static List<FolderDTO> GetFoldersByParentFolderId(FolderDTO dto)
        {
            return MD.DAL.FolderDAO.GetFoldersByParentFolderId(dto);
        }

        public static FolderDTO GetFolderById(FolderDTO dto)
        {
            return MD.DAL.FolderDAO.GetFolderById(dto);
        }

        public static FolderDTO GetFolderByName(FolderDTO dto)
        {
            return MD.DAL.FolderDAO.GetFolderByName(dto);
        }


        public static bool DeleteFolderById(FolderDTO dto)
        {
            return MD.DAL.FolderDAO.DeleteFolderById(dto);
        }

        private static bool DeleteRecursiveFolderById(FolderDTO dto) {
            
            List<FileDTO> fileList = MD.DAL.FileDAO.GetFilesByParentFolderId(((FileFolderItemDTO)dto).ToFileDTO());
            fileList.ForEach((file) => {
                
            });
            return true;
        }

    }
}
