﻿using Assignment_8.Security;
using MD.Entities;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace Assignment_8.Controllers
{
    public class AjaxController : ApiController
    {
        private static String UploadsPath = HttpContext.Current.Server.MapPath("~/uploads/");

        private static Object result(bool IsSuccess, Object Result)
        {
            Object data;
            if (IsSuccess)
            {
                data = new
                {
                    success = true,
                    result = Result
                };
            }
            else
            {
                data = new
                {
                    success = false,
                    Message = (string)Result
                };
            }
            return data;
        }

        [HttpPost]
        public Object GetFoldersByParentFolderId([FromBody] FolderDTO dto)
        {
            List<FolderDTO> folderList = MD.BAL.FolderBO.GetFoldersByParentFolderId(dto);
            if (folderList != null)
            {
                return result(true, folderList);
            }
            else
            {
                return result(false, "Error, there is no parent folder exists.");
            }
        }
		
		[HttpPost]
        public Object GetItemsByParentFolderId([FromBody] FileFolderItemDTO dto)
        {
            List<FileFolderItemDTO> itemList = new List<FileFolderItemDTO>();
            List<FolderDTO> folderList = MD.BAL.FolderBO.GetFoldersByParentFolderId(dto.ToFolderDTO());
            List<FileDTO> fileList = MD.BAL.FileBO.GetFilesByParentFolderId(dto.ToFileDTO());
            if (folderList != null)
                itemList.AddRange(folderList);
            if (fileList != null)
                itemList.AddRange(fileList);

            if (itemList != null)
            {
                return result(true, itemList);
            }
            else
            {
                return result(false, "Error, there is no parent folder exists.");
            }
        }


        [HttpPost]
        public Object GetFolderById([FromBody] FolderDTO dto)
        {
            FolderDTO folder = MD.BAL.FolderBO.GetFolderById(dto);
            if (folder != null)
            {
                return result(true, folder);
            }
            else
            {
                return result(false, "Error, there is no folder exists.");
            }
        }

        [HttpPost]
        public Object GetFolderByName([FromBody] FolderDTO dto)
        {
            FolderDTO folder = MD.BAL.FolderBO.GetFolderByName(dto);
            if (folder != null)
            {
                return result(true, folder);
            }
            else
            {
                return result(false, "Error, there is no folder exists.");
            }
        }

        [HttpPost]
        public Object DeleteFolderById([FromBody] FolderDTO dto)
        {
            bool affected = MD.BAL.FolderBO.DeleteFolderById(dto);
            if (affected)
            {
                return result(true, "Folder Deleted Successfully");
            }
            else
            {
                return result(false, "Error, unable to delete folder");
            }
        }

        [HttpPost]
        public Object SaveFolder([FromBody] FolderDTO dto)
        {
            dto.IsActive = true;
            dto.CreatedOn = DateTime.Now;
            FolderDTO folder = MD.BAL.FolderBO.Save(dto);
            if (folder != null)
            {
                return result(true, folder);
            }
            else
            {
                return result(false, "Error, unable to save folder");
            }
        }

        [HttpPost]
        public Object GetFilesByParentFolderId([FromBody] FileDTO dto)
        {
            List<FileDTO> fileList = MD.BAL.FileBO.GetFilesByParentFolderId(dto);
            if (fileList != null)
            {
                return result(true, fileList);
            }
            else
            {
                return result(false, "Error, there is no parent folder exists.");
            }
        }

        [HttpPost]
        public Object GetFileById([FromBody] FileDTO dto)
        {
            FileDTO file = MD.BAL.FileBO.GetFileById(dto);
            if (file != null)
            {
                return result(true, file);
            }
            else
            {
                return result(false, "Error, there is no folder exists.");
            }
        }

        [HttpPost]
        public Object SaveFile()
        {
            try
            {
                var Request = HttpContext.Current.Request;
                FileDTO fileDto = new FileDTO();
                foreach (var fileName in Request.Files.AllKeys)
                {
                    HttpPostedFile file = Request.Files[fileName];
                    if (file != null)
                    {

                        var rootPath = UploadsPath;
                        if (!Directory.Exists(rootPath))
                        {
                            Directory.CreateDirectory(rootPath);
                        } 
                        

                        fileDto.ParentFolderId = Convert.ToInt32(Request["ParentFolderId"]);
                        fileDto.CreatedBy = Convert.ToInt32(Request["CreatedBy"]);


                        UserDTO userDto = MD.BAL.UserBO.GetUserById(fileDto.CreatedBy);

                        var userUploadPath = System.IO.Path.Combine(UploadsPath, userDto.Login + "/");

                        if (!Directory.Exists(userUploadPath))
                        {
                            Directory.CreateDirectory(userUploadPath);
                        } 


                        fileDto.Name = file.FileName.Substring(0,file.FileName.LastIndexOf('.'));
                        fileDto.FileExt = System.IO.Path.GetExtension(file.FileName);
                        
                        fileDto.UploadedOn = DateTime.Now;
                        var sizeInBytes = file.ContentLength;
                        fileDto.FileSizeInKb = sizeInBytes / 1024;
                        fileDto.IsActive = true;

                        if (MD.BAL.FileBO.Save(fileDto, userUploadPath, file.InputStream) != null)
                        {
                            return result(true, fileDto);
                        }
                    } 
                }

                return result(false, "Error uploading file, invalid file");
            }
            catch (Exception ex)
            {
                return result(false, "Error uploading file:" + ex.Message);
            }
        }


        [HttpPost]
        public Object DeleteFileById([FromBody] FileDTO dto)
        {
            UserDTO userDto = MD.BAL.UserBO.GetUserById(dto.CreatedBy);

            var rootPath = System.IO.Path.Combine(UploadsPath, userDto.Login + "/" + dto.ParentFolderId + "/");

            bool affected = MD.BAL.FileBO.DeleteFileById(dto, rootPath);
            if (affected)
            {
                return result(true, "File Deleted Successfully");
            }
            else
            {
                return result(false, "Error, unable to delete file");
            }
        }

        [HttpGet]
        public HttpResponseMessage DownloadFile([FromUri] FileDTO dto)
        {
            dto = MD.BAL.FileBO.GetFileById(dto);
            UserDTO userDto = MD.BAL.UserBO.GetUserById(dto.CreatedBy);

            var filePath = System.IO.Path.Combine(UploadsPath, userDto.Login + "/" + dto.ParentFolderId + "/" + dto.Name + dto.FileExt);
            var stream = File.ReadAllBytes(filePath);
            // processing the stream.

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream)
            };
            result.Content.Headers.ContentDisposition =
                new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = dto.Name + dto.FileExt
                };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }


        [HttpGet]
        public HttpResponseMessage GeneratePDF([FromUri]int UserId)
        {
            List<FolderDTO> folderList = MD.BAL.FolderBO.GetAllFoldersByUserId(UserId);
            List<FileDTO> filesList = MD.BAL.FileBO.GetAllFilesByUserId(UserId);
            UserDTO userDto = MD.BAL.UserBO.GetUserById(UserId);

            PdfDocument document = new PdfDocument();
            document.Info.Title = "MyDrive Meta Data";
            // Create an empty page
            PdfPage page = document.AddPage();

            // Get an XGraphics object for drawing
            XGraphics gfx = XGraphics.FromPdfPage(page);
            
            // Draw the text
            gfx.DrawString("MyDrive Meta Data", new XFont("Verdana", 20, XFontStyle.BoldItalic), XBrushes.Black,
                new XRect(10, 10, page.Width, page.Height), XStringFormats.TopCenter);


            int height = 55;

            gfx.DrawString("Generated on: " + DateTime.Now.Date.Day + "/" + DateTime.Now.Date.Month + "/" + DateTime.Now.Date.Year, new XFont("Verdana", 10, XFontStyle.Regular), XBrushes.Black, 10, height);
            height += 15;

            gfx.DrawString("Generated by: " + userDto.Name, new XFont("Verdana", 10, XFontStyle.Regular), XBrushes.Black, 10, height);
            height += 30;

            var fontHeading = new XFont("Arial", 20);

            gfx.DrawString("Folders: Total " + folderList.Count, fontHeading, XBrushes.Black, 10, height);
            height += 30;

            //writing folders
            var fontRegular = new XFont("Verdana", 11, XFontStyle.Regular);


            for (int i = 0; i < folderList.Count; i++)
            {
                if (i % 12 == 0 && i != 0)
                {
                    page = document.AddPage();
                    gfx = XGraphics.FromPdfPage(page);
                    height = 15;
                }
                gfx.DrawString("Name: " + folderList[i].Name, fontRegular, XBrushes.Black, 10, height);
                height += 15;
                gfx.DrawString("Type : Folder", fontRegular, XBrushes.Black, 10, height);
                String parentName = "root";

                FolderDTO dto = new FolderDTO();
                dto.Id = folderList[i].ParentFolderId;
                dto.CreatedBy = UserId;
                dto.IsActive = true;
                dto = MD.BAL.FolderBO.GetFolderById(dto);
                if(dto != null)
                    parentName = dto.Name;

                if (folderList[i].ParentFolderId == 0)
                    parentName = "Root";
                height += 15;
                gfx.DrawString("Parent: " + parentName, fontRegular, XBrushes.Black, 10, height);
                height += 25;
            }
            //wrting files now
            page = document.AddPage();
            gfx = XGraphics.FromPdfPage(page);
            gfx.DrawString("Files: Total " + filesList.Count, fontHeading, XBrushes.Black, 10, 30);
            height = 60;
            for (int i = 0; i < filesList.Count; i++)
            {
                if (i % 10 == 0 && i != 0)
                {
                    page = document.AddPage();
                    gfx = XGraphics.FromPdfPage(page);
                    height = 15;
                }
                gfx.DrawString("Name: " + filesList[i].Name, fontRegular, XBrushes.Black, 10, height);
                height += 15;
                gfx.DrawString("File Extension: " + filesList[i].FileExt, fontRegular, XBrushes.Black, 10, height);
                height += 15;
                gfx.DrawString("Type: File", fontRegular, XBrushes.Black, 10, height);
                String parentName = "root";

                FolderDTO dto = new FolderDTO();
                dto.Id = filesList[i].ParentFolderId;
                dto.CreatedBy = UserId;
                dto.IsActive = true;
                dto = MD.BAL.FolderBO.GetFolderById(dto);
                if (dto != null)
                    parentName = dto.Name;


                height += 15;
                gfx.DrawString("Size : " + filesList[i].FileSizeInKb + " KB", fontRegular, XBrushes.Black, 10, height);
                height += 15;
                gfx.DrawString("Parent : " + parentName, fontRegular, XBrushes.Black, 10, height);
                height += 22;
            }


            string filename = "MetaData.pdf";
            var rootPath = System.IO.Path.Combine(UploadsPath, userDto.Login + "/");
            var fileSavePath = System.IO.Path.Combine(rootPath, filename);
            document.Save(fileSavePath);

            byte[] fileBytes = System.IO.File.ReadAllBytes(fileSavePath);

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(fileBytes)
            };
            result.Content.Headers.ContentDisposition =
                new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = filename
                };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");

            return result;

            //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filename);
        }

    }
}
