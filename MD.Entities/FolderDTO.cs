﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MD.Entities
{
    public class FolderDTO : FileFolderItemDTO
    {
        public String type = "folder";
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
    }
}
