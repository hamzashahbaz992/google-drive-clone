﻿using MD.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MD.DAL
{
    public static class FolderDAO
    {
        public static List<FolderDTO> GetAllFoldersByUserId(int UserId)
        {
            var query = String.Format("Select * from dbo.Folders WHERE CreatedBy = {0} AND IsActive = '{1}';", UserId, true);

            using (DBHelper helper = new DBHelper())
            {
                var reader = helper.ExecuteReader(query);
                List<FolderDTO> list = null;
                if (reader.HasRows)
                {
                    FolderDTO dto;
                    list = new List<FolderDTO>();
                    while (reader.Read())
                    {
                        dto = FillDTO(reader);
                        if (dto != null)
                        {
                            if (dto.IsActive)
                                list.Add(dto);
                        }
                    }
                }
                return list;
            }
        }
        public static List<FolderDTO> GetFoldersByParentFolderId(FolderDTO dto)
        {
            var query = String.Format("Select * from dbo.Folders Where ParentFolderId = {0} AND CreatedBy = {1} AND IsActive = '{2}';", dto.ParentFolderId, dto.CreatedBy, dto.IsActive);

            using (DBHelper helper = new DBHelper())
            {
                var reader = helper.ExecuteReader(query);
                List<FolderDTO> list = null;
                if (reader.HasRows)
                {
                    list = new List<FolderDTO>();
                    while (reader.Read())
                    {
                        dto = FillDTO(reader);
                        if (dto != null)
                        {
                            if (dto.IsActive)
                                list.Add(dto);
                        }
                    }
                }
                return list;
            }
        }

        public static FolderDTO GetFolderById(FolderDTO dto)
        {
            var query = String.Format("Select * from dbo.Folders Where Id = {0} AND CreatedBy = {1} AND IsActive = '{2}';", dto.Id, dto.CreatedBy, dto.IsActive);
            using (DBHelper helper = new DBHelper())
            {
                var reader = helper.ExecuteReader(query);
                dto = null;

                if (reader.Read())
                {
                    dto = FillDTO(reader);
                }
                return dto;
            }
        }

        public static FolderDTO GetFolderByName(FolderDTO dto)
        {
            var query = String.Format("Select * from dbo.Folders  Where Name = {0} AND CreatedBy = {1} AND IsActive = '{2}';", dto.Name, dto.CreatedBy, dto.IsActive);
            using (DBHelper helper = new DBHelper())
            {
                var reader = helper.ExecuteReader(query);
                dto = null;

                if (reader.Read())
                {
                    dto = FillDTO(reader);
                }
                return dto;
            }
        }

        public static bool DeleteFolderById(FolderDTO dto)
        {
            var query = String.Format("Update dbo.Folders  Set IsActive = 'False' Where Id = {0} AND CreatedBy = {1} AND IsActive = '{2}';", dto.Id, dto.CreatedBy, dto.IsActive);
            using (DBHelper helper = new DBHelper())
            {
                helper.ExecuteScalar(query);

                return true;
            }
        }

        public static FolderDTO Save(FolderDTO dto)
        {
            var query = "";
            using (DBHelper helper = new DBHelper())
            {
                if (dto.Id > 0)
                {
                    query = String.Format("Update dbo.Folders SET Name = '{0}' WHERE ParentFolderId = {1} AND Id = {2} AND IsActive = '{2}')", dto.Name, dto.ParentFolderId, dto.Id, dto.IsActive);
                    helper.ExecuteScalar(query);
                    return dto;
                }
                else
                {
                    query = String.Format("INSERT INTO dbo.Folders (Name, ParentFolderId, CreatedOn, IsActive, CreatedBy) VALUES ('{0}', {1}, '{2}', '{3}', {4})", dto.Name, dto.ParentFolderId, dto.CreatedOn, dto.IsActive, dto.CreatedBy);
                    query += "; Select Scope_Identity()";
                    dto.Id = Convert.ToInt32(helper.ExecuteScalar(query));
                    return dto;
                }
                    
            }
        }

        private static FolderDTO FillDTO(SqlDataReader reader)
        {
            var dto = new FolderDTO();
            dto.Id = reader.GetInt32(reader.GetOrdinal("Id"));
            dto.Name = reader.GetString(reader.GetOrdinal("Name"));
            dto.ParentFolderId = reader.GetInt32(reader.GetOrdinal("ParentFolderId"));
            dto.CreatedOn = reader.GetDateTime(reader.GetOrdinal("CreatedOn"));
            dto.IsActive = reader.GetBoolean(reader.GetOrdinal("IsActive"));
            dto.CreatedBy = reader.GetInt32(reader.GetOrdinal("CreatedBy"));

            return dto;
        }
    }
}
