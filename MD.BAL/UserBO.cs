﻿using MD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MD.BAL
{
    public class UserBO
    {
        public static List<UserDTO> GetAllUsers()
        {
            return MD.DAL.UserDAO.GetAllUsers();
                
        }

        public static UserDTO ValidateUser(String pLogin, String pPassword)
        {
            return MD.DAL.UserDAO.ValidateUser(pLogin, pPassword);
        }

        public static UserDTO GetUserById(int pId)
        {
            return MD.DAL.UserDAO.GetUserById(pId);
        }
    }
}
