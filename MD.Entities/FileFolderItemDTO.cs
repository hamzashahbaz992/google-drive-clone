﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MD.Entities
{
    public class FileFolderItemDTO
    {
        public String type { get; set; }
        public int Id { get; set; }
        public String Name { get; set; }
        public int ParentFolderId { get; set; }
        public Boolean IsActive { get; set; }
        public int CreatedBy { get; set; }

        public FolderDTO ToFolderDTO()
        {
            FolderDTO dto = new FolderDTO();
            dto.type = "folder";
            dto.Id = Id;
            dto.Name = Name;
            dto.ParentFolderId = ParentFolderId;
            dto.IsActive = IsActive;
            dto.CreatedBy = CreatedBy;
            return dto;
        }
        public FileDTO ToFileDTO()
        {
            FileDTO dto = new FileDTO();
            dto.type = "file";
            dto.Id = Id;
            dto.Name = Name;
            dto.ParentFolderId = ParentFolderId;
            dto.IsActive = IsActive;
            dto.CreatedBy = CreatedBy;
            return dto;
        }

    }

}
