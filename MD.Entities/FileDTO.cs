﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MD.Entities
{
    public class FileDTO : FileFolderItemDTO
    {
        public static List<String> ALLOWED_EXT = new List<String> { ".jpg", ".gif", ".png", ".bmp" };
        public String type = "file";
        public String FileExt { get; set; }
        public int FileSizeInKb { get; set; }
        public DateTime UploadedOn { get; set; }
    }
}
