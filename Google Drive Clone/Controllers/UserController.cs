﻿using Assignment_8.Security;
using MD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_8.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/
        public ActionResult Index()
        {
            if (!SessionManager.IsValidUser)
            {
                return RedirectToAction("Index", "Home", null);
            }
            return View(SessionManager.User);
        }

        [HttpPost]
        public ActionResult ValidateUser(String pLogin, String pPassword)
        {
            UserDTO user = MD.BAL.UserBO.ValidateUser(pLogin, pPassword);
            if (user != null)
            {
                SessionManager.User = user;
                return RedirectToAction("Index");
            }
            else
            {
                TempData["Error"] = "Invalid Login or Password, please try again.";
                return RedirectToAction("Index", "Home", null);
            }
        }


        [HttpPost]
        public ActionResult LogOut()
        {
            if (SessionManager.IsValidUser)
            {
                SessionManager.User = null;
            }

            SessionManager.ClearSession();

            return Redirect("~/Home/Index");
        }
        
	}
}