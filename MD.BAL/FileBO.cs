﻿using MD.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MD.BAL
{
    public class FileBO
    {

        public static List<FileDTO> GetAllFilesByUserId(int UserId)
        {
            return MD.DAL.FileDAO.GetAllFilesByUserId(UserId);
        }
        public static FileDTO Save(FileDTO dto, String rootPath, Stream fileInputStream)
        {
            if (!FileDTO.ALLOWED_EXT.Contains(dto.FileExt))
            {
                return null; // invalid extension
            }
            return MD.DAL.FileDAO.Save(dto,rootPath, fileInputStream);
        }

        public static List<FileDTO> GetFilesByParentFolderId(FileDTO dto)
        {
            return MD.DAL.FileDAO.GetFilesByParentFolderId(dto);
        }


        public static FileDTO GetFileById(FileDTO dto)
        {
            return MD.DAL.FileDAO.GetFileById(dto);
        }

        public static FileDTO GetFileByName(FileDTO dto)
        {
            return MD.DAL.FileDAO.GetFileByName(dto);
        }


        public static bool DeleteFileById(FileDTO dto, string rootPath)
        {
            return MD.DAL.FileDAO.DeleteFileById(dto,rootPath);
        }
    }
}
